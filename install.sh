#!/bin/bash

set -e 

if [[ $# == 1 && $1 == "-f" ]]; then 
   FORCE=1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LINKED=0

for f in $(ls --ignore=install.sh --ignore=config ${DIR}); do
    if [[ ! -e ~/.${f} ]]; then
        ln -s ${DIR}/${f} ~/.${f}
        LINKED=$((LINKED+1))
    elif [[ ! -h ~/.${f} ]]; then
        if [[ $FORCE ]]; then
            rm ~/.${f}
            ln -s ${DIR}/${f} ~/.${f}
            LINKED=$((LINKED+1))
        else
            echo "WARNING: ~/.${f} is a regular file, will not be overwritten..."
        fi
    fi
done 

[[ -d ~/.config ]] || mkdir ~/.config
for f in $(ls ${DIR}/config); do
    if [[ ! -e ~/.config/${f} ]]; then
        ln -s ${DIR}/config/${f} ~/.config/${f}
        LINKED=$((LINKED+1))
    elif [[ ! -h ~/.config/${f} ]]; then
        if [[ $FORCE ]]; then
            rm ~/.config/${f}
            ln -s ${DIR}/config/${f} ~/.config/${f}
            LINKED=$((LINKED+1))
        else
            echo "WARNING: ~/.config/${f} is a regular file, will not be overwritten..."
        fi
    fi
done

if [ -d ${DIR}/vim ]; then
    if [[ ! -e ~/.vimrc ]]; then
        ln -s ${DIR}/vim/vimrc ~/.vimrc
        LINKED=$((LINKED+1))
    elif [[ ! -h ~/.vimrc ]]; then
        if [[ $FORCE ]]; then
            rm ~/.vimrc
            ln -s ${DIR}/vim/vimrc ~/.vimrc
            LINKED=$((LINKED+1))
        else
            echo "WARNING: ~/.vimrc is a regular file, will not be overwritten..."
        fi
    fi
fi

if [[ $LINKED -gt 0 ]]; then
    echo "Updated $LINKED path(s)"
else
    echo "Nothing to be done"
fi
